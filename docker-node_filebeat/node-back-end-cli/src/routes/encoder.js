/**
 * 处理解码器相关的路由
 */

const { SuccessModel, ErrorModel } = require("../model/responseModel.js");
// 引入解码函数
const {
	protocolToRawData
} = require("../controllers/encoder.js");

const handleEncoderRoute = (req, res) => {
  // 定义 处理路由的逻辑
  const method = req.method;

  const id = req.query.id;
  const blogData = req.body;

  // 处理解码逻辑
  if (method === "POST" && req.path === "/api/encoder") {   

    const json = req.query.author || "";

    const DataBytesPromise = protocolToRawData(json);
    return DataBytesPromise.then(DataBytes => {
		if (DataBytes) {
			return new SuccessModel("Success!");
		} else {
			return new ErrorModel("Error!", "/api/encoder");
		}
    })

  }
};

module.exports = handleEncoderRoute;