const  { JSEncrypt } = require('../js_sdk/jsencrypt/index.js')

const publicKey = `MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCiR+TyuZ0xfiBM0JuaQ49BG+pM
jwJYzzeliGbHB0UAgf/G7Y5pMk9VbudyAWDZjinoRSgSTMWgK6X0rkpO4T1bsMzm
MR0jgc6qQaRbzr2AHB9FsUIoRDjCekRK/ScmvSg80o361/jLo2grG7eUrjwyziuE
1jLZgtEAoFZ+PaStjQIDAQAB`

const privateKey = `MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKJH5PK5nTF+IEzQ
m5pDj0Eb6kyPAljPN6WIZscHRQCB/8btjmkyT1Vu53IBYNmOKehFKBJMxaArpfSu
Sk7hPVuwzOYxHSOBzqpBpFvOvYAcH0WxQihEOMJ6REr9Jya9KDzSjfrX+MujaCsb
t5SuPDLOK4TWMtmC0QCgVn49pK2NAgMBAAECgYAvYGfhyPiwY08QvfI/hivDaV/s
u++Gd/CZ/glrX57weANxUxc5dPueSIN/UBGzUgLAEbiSJyGnEjcgCOYewzlDHYL4
7UxF4eXJc1lXP7e1E2ZHWXUHW87DxOGT2+jNXhGgXnhatHZP6b+WwBZJzB+iB1eT
M3X8dYDeN82FuPAEwQJBAM8aDOl7ufQY9ptJTC+OBVOdZ74wo5ZOhpzASxz9knLd
6y3iK/twjlkBG0fu52Z/PFRQ0eV7LeZyu5r+84Fgi70CQQDImLYxmrTeD62BrxDy
FrPcaXqDPxtCpUrQxfCTZHKZ3IQ+xca+AxpoeWLC0UgiluUD8QJ1EfW3WfObmmUX
XF4RAkEAza+gvU9Nmxoj6ow8p0pVDftZ6rGh9wkcdpggwbUx+vVeut5TMlFSfyEz
OBkVJ14cy9zM6i6Roru2ibTGoWsCvQJAHiFyHHlUeZZ5RWV6ciKe2ZiuYlB0U8po
NnvE6x7HZ/0LkDI87AWjTdYN052D7E4mEDV/XP5ZnjTQtqq2TI//QQJBAMTcQIA8
LzxkJJNoKtLInmjKSIiHRh227jg4Jljdv8OAOHlX8nhHhsO70PgNgs2OAamhu2J/
i/S+eWyEuFmaTko=`

// 加密
function encrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPublicKey(publicKey) // 设置公钥
  return encryptor.encrypt(txt) // 对需要加密的数据进行加密
}

// 解密
function decrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPrivateKey(privateKey)
  return encryptor.decrypt(txt)
}

module.exports = {
  encrypt,
  decrypt
}
