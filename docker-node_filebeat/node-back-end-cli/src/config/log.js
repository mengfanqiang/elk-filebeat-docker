//var log4js = require('log4js');

var InternalLogger = function() {
	this.log4js = {};
	this.Logger = {};

	// 对应log4js，可以向控制台输出，也可以保存到滚动文件中，然后可以通过filebeat上传到ELK中
	this.initialize = function() {
		this.log4js = require('log4js');
		this.log4js.configure({
		  appenders: {
			console: {
			  type: 'console'
			},
			file: {
			  type: 'file',
			  filename: '/var/log/node/node.log',
			  maxLogSize: 102400,
			  backups: 3
			}
		  },
		  categories: {
			default: { appenders: ['console', 'file'], level: 'info' }
		  }
		});
		
		this.Logger = this.log4js.getLogger();
	};

	this.trace = function(message) {
		this.Logger.trace(message);
	};

	this.debug = function(message) {
		this.Logger.debug(message);
	};

	this.info = function(message) {
		this.Logger.info(message);
	};

	this.warn = function(message) {
		this.Logger.warn(message);
	};

	this.error = function(message) {
		this.Logger.error(message);
	};

	this.fatal = function(message){
		this.Logger.fatal(message);
	};
};

var Logger = new InternalLogger();
Logger.initialize();

module.exports = Logger;