/*
	定义topic
*/

// 示例
const SELF_DEFINE_TOPIC_UPDATE_FLAG = '/user/update'  //自定义Topic：/user/update。
const SELF_DEFINE_TOPIC_ERROR_FLAG = '/user/update/error' //自定义Topic：/user/update/error。

module.exports = {
  SELF_DEFINE_TOPIC_UPDATE_FLAG, // 示例
  SELF_DEFINE_TOPIC_ERROR_FLAG
};