/*
	定义model
*/

const HEX_DEV_MODEL_BQ71_100_A = 0x0102; // 示例：16进制定义
const IDENTIFIER_DEV_MODEL_BQ71_100_A = "BQ71_100_A"; // 示例：属性唯一标识符

module.exports = {
  HEX_DEV_MODEL_BQ71_100_A,  // 示例
  IDENTIFIER_DEV_MODEL_BQ71_100_A
};