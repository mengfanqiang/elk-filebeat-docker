/*
	定义method
*/

const COMMAND_REPORT = 0x00; //属性上报。
const COMMAND_SET = 0x01; //属性设置。
const COMMAND_REPORT_REPLY = 0x02; //上报数据返回结果。
const COMMAND_SET_REPLY = 0x03; //属性设置设备返回结果。
const COMMAD_UNKOWN = 0xff;    //未知的命令。
const METHOD_PROP_REPORT = 'thing.event.property.post'; //物联网平台Topic，设备上传属性数据到云端。
const METHOD_PROP_SET = 'thing.service.property.set'; //物联网平台Topic，云端下发属性控制指令到设备端。
const METHOD_PROP_SET_REPLY = 'thing.service.property.set'; //物联网平台Topic，设备上报属性设置的结果到云端。

module.exports = {
  COMMAND_REPORT,  // 示例
  COMMAND_SET,
  COMMAND_REPORT_REPLY,
  COMMAND_SET_REPLY,
  COMMAD_UNKOWN,
  METHOD_PROP_REPORT,
  METHOD_PROP_SET,
  METHOD_PROP_SET_REPLY
};