const mysql = require("mysql");
const { MYSQL_CONFIG } = require("../config/db.js");

// 创建连接对象
const connection = mysql.createConnection(MYSQL_CONFIG);

// 开始连接
connection.connect();

// 执行 sql 语句
function execSQL(sql) {

  return new Promise((resolve, reject) => {

    connection.query(sql, (err, result) => {
      if (err) {
        reject(err)
        return;
      }
      resolve(result)
    });
  
  })

}

// 关闭连接, 不然会一直在进程里运行着
// connection.end()

module.exports = {
  execSQL
};
