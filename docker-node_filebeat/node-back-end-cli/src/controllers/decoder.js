// 引入model定义
const {
  BQ_DEV_MODEL_BQ71_100_A,  // 示例
  IDENTIFIER_DEV_MODEL_BQ71_100_A
} = require("../model/model.js");

// 引入topic定义
const {
  SELF_DEFINE_TOPIC_UPDATE_FLAG, // 示例
  SELF_DEFINE_TOPIC_ERROR_FLAG
} = require("../model/topic.js");

// 引入method定义
const {
  COMMAND_REPORT,  // 示例
  COMMAND_SET,
  COMMAND_REPORT_REPLY,
  COMMAND_SET_REPLY,
  COMMAD_UNKOWN,
  METHOD_PROP_REPORT,
  METHOD_PROP_SET,
  METHOD_PROP_SET_REPLY
} = require("../model/method.js");

// 引入辅助解析函数
const {
  bqBytesCrc16,
  bqBytesToString,
  bqBytesToUint8Array,
  bqBytesFromString,
  bqBytesAppendInt8,
  bqBytesAppendInt16,
  bqBytesAppendInt32
} = require("../model/3rdModel.js");

// 对外接口：接收序列化数据转换为JSON
function rawDataToProtocol(bytes) {
    var uint8Array = new Uint8Array(bytes.length);
    for (var i = 0; i < bytes.length; i++) {
        uint8Array[i] = bytes[i] & 0xff;
    }
    var dataView = new DataView(uint8Array.buffer, 0);
    var jsonMap = new Object();
    var fHead = uint8Array[0]; // command
    if (fHead == COMMAND_REPORT) {
		// jsonMap['method']中'method'为model.js中定义的内容
        jsonMap['method'] = METHOD_PROP_REPORT; //ALink JSON格式，属性上报topic。
        jsonMap['version'] = '1.0'; //ALink JSON格式，协议版本号固定字段。
        jsonMap['id'] = '' + dataView.getInt32(1); //ALink JSON格式，标示该次请求id值。
        var params = {};
        params['prop_int16'] = dataView.getInt16(5); //对应产品属性中prop_int16。
        params['prop_bool'] = uint8Array[7]; //对应产品属性中prop_bool。
        params['prop_float'] = dataView.getFloat32(8); //对应产品属性中prop_float。
        jsonMap['params'] = params; //ALink JSON格式，params标准字段。
    } else if(fHead == COMMAND_SET_REPLY) {
        jsonMap['version'] = '1.0'; //ALink JSON格式，协议版本号固定字段。
        jsonMap['id'] = '' + dataView.getInt32(1); //ALink JSON格式，标示该次请求id值。
        jsonMap['code'] = ''+ dataView.getUint8(5);
        jsonMap['data'] = {};
    }

    return jsonMap;
}

// 接收序列化数据转换为JSON（协议头）
function rawDataToProtocol_header(bytes) {
	let jsonMap = {}
	jsonMap.params = {}
	
	try {
		// processData function
	} catch (e) {
		// console.log("rawDataToProtocol", e)
	}
	return jsonMap
}

// 接收序列化数据转换为JSON（属性）
function rawDataToProtocol_attribute(bytes) {
	let jsonMap = {}
	jsonMap.params = {}
	
	try {
		// processData function
	} catch (e) {
		// console.log("rawDataToProtocol", e)
	}
	return jsonMap
}

// 接收序列化数据转换为JSON（下行 - 服务）
function rawDataToProtocol_service(bytes) {
	let jsonMap = {}
	jsonMap.params = {}
	
	try {
		// processData function
	} catch (e) {
		// console.log("rawDataToProtocol", e)
	}
	return jsonMap
}

// 接收序列化数据转换为JSON（上行 - 信息/报警/故障上报）
function rawDataToProtocol_event(bytes) {
	let jsonMap = {}
	jsonMap.params = {}
	
	try {
		// processData function
	} catch (e) {
		// console.log("rawDataToProtocol", e)
	}
	return jsonMap
}

// 接收序列化数据转换为JSON（上行 - 心跳保活）
function rawDataToProtocol_heat(bytes) {
	let jsonMap = {}
	jsonMap.params = {}
	
	try {
		// processData function
	} catch (e) {
		// console.log("rawDataToProtocol", e)
	}
	return jsonMap
}

// 接收序列化数据转换为JSON（上行 - 设备注册）
function rawDataToProtocol_register(bytes) {
	let jsonMap = {}
	jsonMap.params = {}
	
	try {
		// processData function
	} catch (e) {
		// console.log("rawDataToProtocol", e)
	}
	return jsonMap
}

module.exports = {
  rawDataToProtocol
};